# Intégration continue

L'intégration continue est un aspect plus avancé et n'est pas nécessaire pour tous les projets. Néanmoins, pour les projets de production la mise en place d'une intégration continue précoce peut s'avérer utile pour renforcer la reproductibilité des traitements.  

L’intégration continue est un ensemble de pratiques consistant à vérifier à chaque modification de code source que le résultat des modifications ne produit pas de régression dans l’application développée. Le principal but de cette pratique est de détecter les problèmes d’intégration au plus tôt lors du développement. De plus, elle permet d’automatiser l’exécution des suites de tests, la compilation d'une documentation ou voir l’évolution du développement de la chaîne de traitement.

La formation *Travail Collaboratif sous R* propose quelques exemples de fichiers de configuration `.gitlab-ci.yml` types pour des chaînes de traitement `R`. 