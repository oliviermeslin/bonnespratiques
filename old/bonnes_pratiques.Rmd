---
title: "Bonnes pratiques en R"
author: 
   - Raphaële Adjerad$^1$
   - Mathias André$^2$
   - Lino Galiana$^2$
   - $^1$ Drees
   - $^2$ Insee
date: "`r format(Sys.Date(), format = '%d %B %Y')`"
output:
  html_document:
    css: style.css
    toc: true
    code_folding: show
    number_sections: true
    includes:
      in_header: logo.html
---

<!--------------------------------------------------
INCLUDE JAVA SCRIPT TO CONTROL OUTPUT SHOWING

From  https://stackoverflow.com/questions/37755037/how-to-add-code-folding-to-output-chunks-in-rmarkdown-html-documents
---------------------------------------------------->

<script src="js/hideOutput.js"></script>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Mise en place de l'environnement dans R Studio

<span style="color:DarkBlue">**Editeur**</span>: Privilégier l'éditeur `R studio` qui est le mieux intégré à `R`, bien intégré à d'autres langages, très complet (auto-complétion, debuggeur...), dispose d'*addins*...

<span style="color:DarkBlue">**Encoding**</span>: Dans `Tools > Global options > Code > Saving`, choisir le Default text encoding **UTF-8** pour les caractères spéciaux. Sans encoding UTF-8, les caractères spéciaux (accents...) risquent d'être formatés de manière peu compréhensible

<span style="color:DarkBlue">**Workspace**</span>: Dans les paramètres par défaut, il faut mettre que le workspace commence vide. `Tools > global options > General`  et vérifier que *"Restore R.Data in workspace at start up"* est bien décoché. Cela est dû au fait que l'on souhaite un code reproductible, commencer avec des données parfois déjà traitées et non brutes peut amener à ne pas avoir un code reproductible.

## Projets R

<span style="color:DarkBlue">**Projet R**</span>: Organiser son travail (seul ou en équipe) avec des projets R. Pour créer un projet, `File > New project`

Le `R Project` permet un enregistrement d'un fichier historique des commandes dans les scripts du projet (fichier `.Rhistory`). Par ailleurs, le *working directory* est celui du chemin du projet (s'il n'est pas modifié manuellement), les scripts en cours d'édition sont mémorisés, etc.

Il peut être intéressant pour bien organiser son travail de diviser les dossiers à l'intérieur du R project en des dossiers: "donnees", "output", "scripts", etc.

<span style="color:DarkBlue">**Structuration sous forme d'un package**</span>: Un *package* est une généralisation du principe des projets `R` où sont gérés de manière formelle et cohérente les dépendances internes (au sein du package) et externes (avec les autres packages) entre objets, fonctions et méthodes. Il s'agit de la meilleure manière d'assurer la reproducibilité du code tout en assurant sa documentation. Un projet de production devrait être systématiquement associé à un ou plusieurs packages documentant les traitements effectués. Le livre [R Packages](http://r-pkgs.had.co.nz/) est une source instructive et accessible sur le sujet.




# Nommage

Attention R est sensible à la casse. Un code écrit avec des noms de variables et de fonctions explicites est aussi, voire plus, informatif que les commentaires qui l’accompagnent. 

Les fonctions de base `R` utilisent des points dans les noms de fonctions ( `contrib.url()` par exemple) ou les noms de classes (`data.frame`). Il est néanmoins mieux de réserver les points uniquement aux objets de classe `S3` sous la forme `fonction.classe`

Si vous vous retrouvez à avoir des variables `model_2018`, `model_2019`, `model_2020`, il est probablement plus judicieux de créer une liste dont chaque élement est une année. Par exemple,

<div class="fold o">
```{r}
# PAS BIEN
data_2018 <- data.frame(x = runif(3))
data_2019 <- data.frame(x = runif(3))
data_2020 <- data.frame(x = runif(3))

# BIEN
data <- list(
  "2018" = data.frame(x = runif(3)),
  "2019" = data.frame(x = runif(3)),
  "2020" = data.frame(x = runif(3))
)
data

# BIEN AUSSI
data <- do.call(
  rbind, list(
    data.frame(x = runif(3), year = 2018),
    data.frame(x = runif(3), year = 2019),
    data.frame(x = runif(3), year = 2020))
)
data
```
</div>

## Nommage des fichiers

<span style="color:DarkBlue">**Extensions**</span> Pour enregistrer les fichiers, l'extension pour les scripts est `.R`, et pour les fichiers de données rdata, `.RData` (faire attention à la casse). On peut aussi enregistrer un objet R dans un format `.rds`. Les fichiers `R markdown` sont enregistrés sous l'extension `.Rmd`

<span style="color:DarkBlue">**Versions**</span>: En l'absence de système de contrôle de version (`git` ou `SVN`), millésimer les fichiers (scripts et données). Pour les données, on millesimera systématiquement avec la date (pour les agents de la Drees, la fonction `rdrees::exporter_data` propose de le faire automatiquement). L'utilisation de contrôle de version avec `git` est recommandé et est très bien intégré avec `Rstudio`. L'intérêt de `git` est que cela permet un suivi des évolutions du fichier bloc par bloc. Dans ce cas, il n'est pas nécessaire de millésimer les fichiers, l'historique des modifications d'un fichier étant géré par `git` (cf. Section [Contrôle de version](#git)). Pour le versioning, on pourra également utiliser le gitlab de la Drees ou de l'Insee. 

<span style="color:DarkBlue">**Organisation**</span>: Pour les scripts, on préférera en préfixe indiquer le numéro du script dans la série de scripts à exécuter (si cela s'applique) et en suffixe le millesimage. Pour les scripts en développement, on pourra ajouter le suffixe *"\_dev"* et pour les scripts utilisés en production (à ne pas modifier), on pourra utiliser le suffixe *"\_prod"*. Ainsi pour un script le nom sera par exemple `1_import_donnees_vdev_20190417.R`. Le 1 indique le numéro du script, c'est le premier à exécuter dans le projet. 

Pour millesimer manuellement on pourra utiliser le type de script suivant:

```{r exemple millesime, eval = F}
# parametres ---------------------------------------------
date <- Sys.Date()
chemin_donnees <- "I:/.../"
# code 
#...


# enregistrement -----------------------------------------
save(fichier, file = paste0(chemin_donnees, "fichier_", date, ".RData"))

```

On pourra sauvegarder des objets intermédiaires au cours du script. Pour s'assurer de la reproductibilité des scripts, l'intégralité du code devra être réexécuté in fine. 

Ne pas utiliser de symboles spéciaux dans le nom du fichier (-, ., caractères accentués, etc.)

Privilégier les *underscore* (par exemple `donnees_menages`) au CamelCase (consiste à donner un nom en mettant une majuscule à chaque début de mot : par exemple `donneesMenages`). Si vous préférez malgré cela le camelCase, utilisez le dans tout le script (uniformisation du code).


## Nommage des variables

Pour une variable, il vaut mieux choisir des substantifs. Privilégier les *underscore* (par exemple `donnees_menages`) au CamelCase (par exemple `donneesMenages`). Si vous préférez le camelCase, utilisez le dans tout le script (uniformisation du code).

**Ne pas donner comme noms T ou F pour des variables** (car ce sont les booléens TRUE et FALSE) **ou bien des noms qui sont déjà des fonctions de base R** (`mean` par exemple). Cela ne génère pas d'erreur mais cela est très important pour ne pas avoir un code qui peut aboutir à des erreurs non forcément détectables!

```{r exemple T}

# On commence avec équivalence TRUE et T
TRUE == T
2 == TRUE

# On crée une variable T à un moment (shortcut de TRUE)
T <- 2

# On a rompu l'équivalence entre T et TRUE
TRUE == T
2 == T
```


## Nommage des fonctions

<span style="color:DarkBlue">**Nom de la fonction**</span> L'usage des verbes est à privilégier. Essayer de nommer une fonction en fonction de ce qu'elle fait. Privilégier les underscore (par exemple calculer_taxes) au CamelCase (par exemple calculerTaxes). Si vous préférez le camelCase, utilisez le dans tout le script (uniformisation du code).

<span style="color:DarkBlue">**Nom des arguments**</span> L'ordre des arguments est à respecter si on ne les nomme pas dans l'appel à une fonction. Il est donc préférable de nommer les arguments explicitement, sauf dans les cas où il ne peut y avoir d'équivoque. Par exemple, avec la fonction `base::gsub()`, on nommera explicitement les arguments `pattern = ..., replacement = ...,  x = ...`, etc. 

Par exemple, supposons qu'on appelle la fonction `gsub` mais qu'on se trompe d'ordre dans les arguments de la fonction. Alors on ne remplacera pas *toto* par *cool* comme voulu mais aura un comportement difficile à anticiper:

```{r exemple confusion arguments}
my_awesome_func <- function(x = "super toto", replacement = "cool", pattern = "toto"){
  gsub(x,replacement,pattern)
}
my_awesome_func()

my_awesome_func2 <- function(x = "super toto", replacement = "cool", pattern = "toto"){
  gsub(x,replacement = replacement,pattern = pattern)
}
my_awesome_func2()
```

Comme pour les variables, ne pas donner comme noms `T` ou `F` pour des fonctions (car ce sont les booléens `TRUE` et `FALSE`) ou bien des noms qui sont déjà des fonctions de base R (`mean` par exemple).

# Syntaxe

## Longueur des lignes

La longueur maximale recommandée d'une ligne est 80 caractères, il faut bien retourner à la ligne pour que le code soit lisible. 

Par exemple,

```{r, eval = FALSE}
# Good
do_something_very_complicated(
  something = "that",
  requires = many,
  arguments = "some of which may be long"
)

# Bad
do_something_very_complicated("that", requires, many, arguments,
                              "some of which may be long"
)
```

## Indentation

`R`, contrairement à `Python`, ne provoque pas d'erreur en cas de mauvaise indentation ou d'absence d'indentation. Néanmoins, un code bien indenté est plus facile à lire et à faire évoluer.

Par défaut, dans `Tools > Global Options > code, "Insert spaces for tab"` est coché (le vérifier). Cela signifie que l'on peut utiliser la touche tab, mais qu'elle sera remplacée par des espaces. Lorsque l'on navigue dans le code, ces espaces se comportent comme des tabulations. 

Le raccourci `Ctrl+i` ré-indente les lignes de code sélectionnées.

```{r, eval = FALSE}
# Pas bien: difficile a lire
if (a>0){
b <- a^2
if (b>4){
c <- log(b)
}else{
c <- exp(b)
}
}else{
b <- -a^2 + 3
c <- NULL
}

# Bien: lisible (suffit de faire CTRL+I)
if (a>0){
  b <- a^2
  if (b>4){
    c <- log(b)
  }else{
    c <- exp(b)
  }
}else{
  b <- -a^2 + 3
  c <- NULL
}
```

## Espacement

Les opérateurs binaires (`=`, `+`, `-`, `<-`, etc.) sont entourés d'espaces avant et après pour plus de lisibilité.

Placer un espace après les virgules, pas avant, comme en Français

```{r, eval = FALSE}
# Good
x[, 1]

# Bad
x[,1]
x[ ,1]
x[ , 1]
```

Ne pas mettre d'espace avant ou après les parenthèses

```{r, eval = FALSE}
# Good
mean(x, na.rm = TRUE)

# Bad
mean (x, na.rm = TRUE)
mean( x, na.rm = TRUE )
```

Les opérateurs de comparaison doivent être entourés d'espace pour ne pas être mal-interprétés par `R`

```{r, error = TRUE}

# ESPACE: COMPARAISON COMPRISE
3 < -5

# PAS D'ESPACE: CROIT A UNE ASSIGNATION
3 <-5

```

Les exceptions à la règle des espaces sont `:`, `::` et `:::`, ainsi que `$`, `@` ou les symboles pour sélectionner une sous-partie d'un objet, par exemple `[[` et `]]` pour les listes


## Accolades

La ligne contenant une commande `if` doit posséder une accolade ouvrante *« { »*.	La commande `else` doit toujours être entourée d’une accolade fermante et d’une accolade ouvrante. Cela permet de ne pas générer de *bug* lors d'une exécution ligne à ligne. 

## Affectation

Dans beaucoup de contextes, les opérateurs d'assignation `<-` et `=` produisent le même résultat. Néanmoins, il est préférable d'utiliser `<-` qui est plus cohérent, en `R`. L'opérateur `=` est réservé pour les expressions booléennes, les définitions d'arguments à l'intérieur d'une fonction, etc.

## Règle concernant l'usage des `pipes`

On recommande de passer à la ligne systématiquement après chaque pipe (`%>%`) pour plus de lisibilité.


# Organisation du code

## Lister les dépendances de package

Mettre en exergue la liste des packages utilisés, la définition des fonctions (si elles sont utilisées plusieurs fois dans le script) et la définition des paramètres. Cela permet de bien visualiser les packages chargés pour un script ainsi que l'ensemble des paramètres à éventuellement modifier/mettre à jour. 

On peut mettre en place les lignes ci-dessous pour installer automatiquement une série de *packages* si ceux-ci ne sont pas déjà installés:

```{r, eval = FALSE}
list.of.packages <- c("ggplot2", "Rcpp")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
```

Ce code ne charge pas les packages, il faut ajouter `library` s'il est nécessaire de charger le package pour exécuter le code (si toutes les fonctions ont été importées sous la forme `pkg::function`, par exemple `dplyr::select`, ce n'est pas nécessaire)

## Titres

Mettre des titres aux sous parties du code pour plus de lisibilité du script,  de la forme (section) : 
```{r eval = F}
# Titre 1 ------------------------------------------
# Titre 2 ========================

# ou bien 

##### Titre 1 ------------------------------------
##### Titre 2 ====================================
```

Il est alors possible de se rendre directement à une section à partir de la fenêtre script (en bas avec les flêches).

## Commentaires

Il faut bien commenter son code pour qu'il soit repris par autrui ou par soi-même dans le futur plus facilement. Il faut veiller à ce que les commentaires soient clairs et relativement courts

## Documenter les fonctions

Une bonne pratique est de documenter les fonctions selon le standard `Roxygen2` qui est un package proposant des tags standardisés `@` pour documenter une fonction. Les documentations `Roxygen2` peuvent être reconnues grâce à `#'`. En développement de *package*, cela permet de générer les documentations accessibles en tapant `?*`

Documenter une fonction consiste, en premier lieu, à expliquer l'objet de celle-ci, de manière succincte ou détaillée, les paramètres autorisés et le résultat. `Roxygen2` propose de nombreux tags pour documenter de manière plus détaillée une fonction, une documentation plus abondante est disponible [ici](http://r-pkgs.had.co.nz/man.html).


```{r, eval = FALSE}
#' Add together two numbers.
#' 
#' @param x A number.
#' @param y A number.
#' @return The sum of \code{x} and \code{y}.
#' @examples
#' add(1, 1)
#' add(10, 1)
add <- function(x, y) {
  x + y
}
```

La documentation des fonctions utilisées est fondamental pour assurer la reproductibilité du code et la possibilité d'évolutions futures. Que l'on travaille en équipe ou tout seul, documenter les fonctions de cette manière réduit les risques d'oubli ou de confusion. 

La documentation d'un package est une étape essentielle, mais elle ne se suffit pas à elle-même. Pour le développement de packages, il est important également d'avoir recours à des tests unitaires afin d'assurer la fiabilité du package tout au long du développement.

## Paramétrage

Pour que la reprise du code par autrui soit facilitée, mettre les données brutes utilisées dans un calcul dans des paramètres en début de script. 

Par exemple, si l'on utilise un paramètre fiscal, type taux d'une taxe:

```{r eval = F}
# Avec la liste des paramètres en début de code
taux_taxe <- 0.0627
assiette_taxe <- 1

# ...
# On utilise directement la variable taux_taxe dans le calcul.
impot <- assiette_taxe*taux_taxe
```

Les paramètres doivent être définis en début de script, y compris s'ils sont définis à partir d'un fichier excel importé, afin de pouvoir les visualiser rapidement pour éventuellement les modifier/actualiser.


## Chemins

* Si le code appartient à un `RProject`, définir les chemins de manière relative sur tous les éléments qui appartiennent au projet. Par exemple, si les données sont dans le dossier `C:/MesDocuments/monprojet/data`, un projet dans `C:/MesDocuments/monprojet` permettra de faire seulement référence à `./data` 
* Sinon, définir le chemin principal en début de code `chemin_principal <- "C:/MesDocuments/monprojet"` et faire `setwd(chemin_principal)` pour pouvoir faire des chemins relatifs


```{r eval = F}
# Avec la liste des chemins en début de code

chemin_donnees <- "I:/ECHANGE/.../"

# ...

# Quand on veut appeler le fichier
load(paste0(chemin_donnees, "nom_script.R"))

```

L'utilisation des projets *Rstudio* est une pratique à privilégier car elle assure une meilleure reproducibilité du code


# Programmes `R`

## Gestion des conflits entre packages

Lorsqu'on utilise une fonction issue d'un package, il est recommandé de mettre le nom du package suivi de `::`. Cela alourdit quelque peu le code mais permet à la personne reprenant celui-ci de comprendre quelle fonction issue de quel package est utilisée. En effet, cela permet d'être certain que l'on exécute le même script que la personne ayant écrit le code, indépendamment de son environnement. Par exemple : 

<div class="fold o">
```{r eval = TRUE, error=TRUE}
library(dplyr)
library(MASS)
cars %>% select(dist)
```
</div>
produira une erreur alors que : 
<div class="fold o">
```{r eval = TRUE}
# On detache les packages pour repartir d'un environnement propre
detach("package:MASS", unload=TRUE)
detach("package:dplyr", unload=TRUE)

# On attache les packages dans ce sens
library(MASS)
library(dplyr)
cars %>% select(dist) %>% head()
```
</div>
fonctionnera bien.

Pour éviter cela, 
<div class="fold o">
```{r eval = T}
cars %>% dplyr::select(dist)  %>% head()
```
</div>
fonctionnera quels que soient les packages chargés en mémoire et l'ordre dans lequel ils l'ont été.

<span style="color:DarkBlue">**Pour les développeurs de package**</span>  `::` évite de charger un package. En développement de package on privilégiera systématiquement cette forme. Les packages appelés sous cette forme sont à mettre dans le champ `Imports` et, il est bon, de n'avoir aucun package dans le champ `Depends`

<span style="color:DarkBlue">**En production**</span> L'écriture rigoureuse `::` est particulièrement importante en production où le code doit être robuste quelque soit l'environnement d'un utilisateur. Une bonne pratique est de systématiquement associer une chaîne de production à une série de fonctions gérées sous forme de package (cf. formation INSEE <span style="color:DarkBlue">**Travail collaboratif sous R**</span>)

Les utilisateurs de `Python` sont plus habitués à cette syntaxe rigoureuse:

```{r, eval = FALSE}
# Code Python
import numpy as np
np.array([1, 2, 3])
```
Il n'y a pas d'ambiguité ici: `np.array` renvoie à la fonction `array` dans l'espace `np` qui est un raccourci comme on l'a déclaré en ligne 1 de `numpy`. Adopter une approche similaire en `R` avec les `::` est fondamental en équipe. 


## Packages à privilégier

Pour la manipulation de données, les fonctions du `tidyverse`, dont l’objectif est de rendre le code lisible au plus grand nombre, pourront être privilégiées. Toutefois, l’utilisation d’autres packages peuvent également s’avérer utiles ou nécessaires.

Par exemple, pour le traitement de bases plus volumineuses, l’utilisation de `data.table`, souvent plus efficace et moins chronophage pour traiter de telles données, pourra être préférable.

Le `tidyverse` n'est, à l'origine, pas une syntaxe pour développer des fonctions. Pour l’implémentation de fonctions et de chaînes de traitement, il pourra être moins complexe d’utiliser les fonctions de bases R ou les données sous format `data.table`, la manipulation des fonctions du `tidyverse` dans des fonctions pouvant être difficile à appréhender.

Privilégier l'utilisation de packages stabilisés, pour avoir un code reproductible (y.c. quand une nouvelle version de R, de nouvelles versions des packages apparaissent).Le `tidyverse` est, à l'heure actuelle, un environnement peu stable donc à utiliser avec précaution. 


## Privilégier une écriture vectorielle

`R` est un langage vectoriel, c'est-à-dire que les fonctions sont optimisées pour s'appliquer à des vecteurs ou dataframe/matrices (qui sont une collection de vecteurs de taille identique). Il est donc préférable d'éviter les boucles en `R` qui ne sont pas adaptées à ce langage. Il faut **tirer au maximum parti de la vectorialisation de R**.

Lorsqu'on doit appliquer de multiples reprises une fonction, on utilisera les fonctions de la famille `*apply`, par exemple `lapply`, plutôt qu'une boucle `for`.


## Gérer la casse

Si vous ne souhaitez pas avoir trop de problème de casse, choisissez la norme de code en underscore (`_`) et lorsque vous importez des données, transformez tous les noms de variables en minuscules (à l'aide par exemple de la commande `tolower`).

## A éviter

* ne pas utiliser `attach` et `detach`, on retrouve parfois ce type de gestion de base dans des exemples mais c'est assez mauvais pour un statisticien-économiste qui doit souvent gérer plusieurs bases à la fois (dont les noms des variables sont parfois identiques)
* ne pas utiliser `eval(parse(.))` (si vous retrouvez ce genre de recommandation dans des forums, essayez de l'éviter dans la plupart des cas). En effet, en général il existe une alternative à cette syntaxe qui permet d'utiliser une syntaxe plus claire de R (en faisant appel à des caractéristiques spécifiques des structures de données en R). Une raison toute simple qui incite à ne pas utiliser cette syntaxe par exemple est qu'il est plus difficile a priori de debugger un code utilisant `eval(parse(.))` qu'un code utilisant les structures de données R. Le `eval(parse(.))` est également plus difficile à lire pour quelqu'un qui reprendrait le code (c'est une écriture assez lourde).  
* quand on fait du *subsetting* en base R, c'est à dire lorsque l'on sélectionne des lignes/colonnes spécifiques grâce à des conditions booléennes, il faut éviter d'imbriquer de nombreuses conditions, cela devient rapidement illisible.


# Outils à disposition pour s'assurer du respect des bonnes pratiques

Des packages ou *addins* Rstudio ont été développés pour évaluer la qualité de la syntaxe de codes intégrés dans un projet. Les packages `styler` et `goodpractice` évaluent la qualité d'un code suivant quelques règles simples. 

En développement de package, on pourra utiliser la fonction `devtools::check()` qui effectue environ 80 tests pour assurer la robustesse du code. Les problèmes potentiels sont alors signalées selon leur caractère critique: les `erreurs` et `warnings` sont des problèmes de robustesse préoccupants, les `notes` sont des problèmes secondaires mais pouvant affecter la robustesse du code. 

## Package `goodpractice`

Supposons qu'on ait un package dont on souhaite assurer la diffusion. Le package `goodpractice` permet de lister les écarts aux bonnes pratiques. Voici un exemple de sortie `goodpractice`:

```{r, eval = FALSE}
g <- goodpractice::gp(mypackage)
g

#> ── GP badpackage ──────────────────────────────────────────────────────────
#> 
#> It is good practice to
#> 
#>   ✖ not use "Depends" in DESCRIPTION, as it can cause name
#>     clashes, and poor interaction with other packages. Use
#>     "Imports" instead.
#>   ✖ omit "Date" in DESCRIPTION. It is not required and it gets
#>     invalid quite often. A build date will be added to the package
#>     when you perform `R CMD build` on it.
#>   ✖ add a "URL" field to DESCRIPTION. It helps users find
#>     information about your package online. If your package does
#>     not have a homepage, add an URL to GitHub, or the CRAN package
#>     package page.
#>   ✖ add a "BugReports" field to DESCRIPTION, and point it to a bug
#>     tracker. Many online code hosting services provide bug
#>     trackers for free, https://github.com, https://gitlab.com,
#>     etc.
#>   ✖ omit trailing semicolons from code lines. They are not needed
#>     and most R coding standards forbid them
#> 
#>     R/semicolons.R:4:30
#>     R/semicolons.R:5:29
#>     R/semicolons.R:9:38
#> 
#>   ✖ not import packages as a whole, as this can cause name clashes
#>     between the imported packages. Instead, import only the
#>     specific functions you need.
#>   ✖ fix this R CMD check ERROR: VignetteBuilder package not
#>     declared: ‘knitr’ See section ‘The DESCRIPTION file’ in the
#>     ‘Writing R Extensions’ manual.
#>   ✖ avoid 'T' and 'F', as they are just variables which are set to
#>     the logicals 'TRUE' and 'FALSE' by default, but are not
#>     reserved words and hence can be overwritten by the user.
#>     Hence, one should always use 'TRUE' and 'FALSE' for the
#>     logicals.
#> 
#>     R/tf.R:NA:NA
#>     R/tf.R:NA:NA
#>     R/tf.R:NA:NA
#>     R/tf.R:NA:NA
#>     R/tf.R:NA:NA
#>     ... and 4 more lines
#> 
#> ───────────────────────────────────────────────────────────────────────────
```

# Contrôle de version

# Intégration continue