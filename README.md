# Guide des bonnes pratiques

[![pipeline status](https://gitlab.com/linogaliana/bonnespratiques/badges/master/pipeline.svg)](https://gitlab.com/linogaliana/bonnespratiques/-/commits/master)

Un guide des bonnes pratiques construit entre l'`Insee` et la `Drees`. Les supports sont déployés [ici](https://linogaliana.gitlab.io/bonnespratiques)